<?php

// ? Abstract Class

// * Sebuah class yang tidak dapat di-instansiasi
// * Kelas 'abstrak'
// * Mendefinisikan interface untuk kelas lain yang menjadi turunan-nya
// * Berperan sebagai 'kerangka dasar' untuk kelas turunan-nya
// * Memiliki minimal 1 method abstrak
// * Digunakan dalam pewarisan untuk memaksakan implementasi method abstrak yang sama untuk semua kelas turunan-nya
// * Semua kelas turunan, harus mengimplementasikan method abstrak yang ada di kelas abstraknya
// * Kelas abstrak boleh memiliki property / method regular
// * Kelas abstrak boleh memiliki property / static method

// ? Kenapa menggunakan abstrak class ?

// * Merepresentasikan ide atau konsep dasar
// * Composition over inheritance
// * Salah satu cara menerapkan Polimorphism
// * Sentralisasi logic
// * Mempermudah pengerjaan tim
?>
