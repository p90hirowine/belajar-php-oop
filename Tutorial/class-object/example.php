<?php

// ? Pengertian Class

// * Blueprint atau templete atau wadah untuk membuat instance dari object
// * Class mendefinisikan object
// * Menyimpan data dan perilaku yang disebut dengan propety dan methode
// * Class dibuat dengan menggunakan keyword 'class'

// ? Cara membuat class

class Coba
{
    public $a; // property

    // methode
    public function b()
    {
    }
}


// ? Pengertian object

// * Instance yang didefinisikan oleh object
// * Implementasi kongkret dari sebuah class
// * Banyak object dapat dibuat menggunakan satu class
// * Object dibuat dengan mengggunakan keyword 'new'

// ? Cara membuat object

class Test
{
    public $nama = "Randie";

    public function hello()
    {
        echo "Selamat pagi ! $this->nama";
    }
}

// membuat object instance dari class
$a = new Test();
$b = new Test();

print_r($a);
echo "</br>";
print_r($b);

$b->hello();
