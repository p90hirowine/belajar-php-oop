<?php

// ? Interface

// * Kelas abstrak yang sama sekali tidak memiliki implementasi
// * Murni merupakan template untuk kelas turunan-nya
// * Tidak boleh memiliki property, hanya deklarasi method saja
// * Semua method harus dideklarasikan dengan visibility public
// * Boleh mendeklarasikan __construct()
// * Satu class boleh mengimplementasikan banyak interface
// * Dengam menggunakan type-hinting dapat melakukan 'Dependency Injection'
// * Pada akhirnya akan mencapai Polymorphism
?>
