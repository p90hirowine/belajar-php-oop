<?php

class Produk
{
	public $judul, $penulis, $penerbit, $harga;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
	{
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getLabel()
	{
		return "$this->penulis, $this->penerbit";
	}
}

$produk3 = new Produk("Naruto", "Masashi Kishimoto", "Shounen Jump", 30000);

// var_dump($produk3);

echo "Komik : " . $produk3->getLabel();
echo "</br>";

$produk4 = new Produk("Uncharted", "Neil Druckmann", "Sonny Computer", 250000);

// var_dump($produk4);

echo "Game : " . $produk4->getLabel();
echo "</br>";

class CetakInfoProduk
{
	public function cetak(Produk $produk)
	{
		$str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";

		return $str;
	}
}

$infoProduk1 = new CetakInfoProduk();
echo $infoProduk1->cetak($produk3);
