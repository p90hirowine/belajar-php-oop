<?php

// ? Static

// * Class merupakan template dari object
// * kita bisa mengakses property dan method dalam konteks class
// * Member yang terikat dengan class, bukan dengan object
// * Nilai static akan selalu tetap meskipun object di instansiasi berulang kali

// class ContohStatic
// {
// 	public static $angka = 1;

// 	public static function halo()
// 	{
// 		return "Halo ... " . self::$angka++ . " kali";
// 	}
// }

// echo ContohStatic::$angka;
// echo "</br>";
// echo ContohStatic::halo();

// echo "<hr>";

// echo ContohStatic::halo();

class Example
{
	public static $angka = 1;

	public function halo()
	{
		return "Halo " . self::$angka++ . " kali </br>";
	}
}

$obj = new Example();
echo $obj->halo();
echo $obj->halo();
echo $obj->halo();

echo "<hr>";

$obj2 = new Example();
echo $obj2->halo();
echo $obj2->halo();
echo $obj2->halo();

?>
