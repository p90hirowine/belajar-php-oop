<?php

class Produk
{
	public $judul, $penulis, $penerbit, $harga;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
	{
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getLabel()
	{
		return "$this->penulis, $this->penerbit";
	}
}

$produk3 = new Produk("Naruto", "Masashi Kishimoto", "Shounen Jump", 30000);
// $produk3->judul = "Naruto";
// $produk3->penulis = "Masashi Kishimoto";
// $produk3->penerbit = "Shounen Jump";
// $produk3->harga = 30000;

var_dump($produk3);

// echo "Komik : $produk3->penulis, $produk3->penerbit";
echo "</br>";
echo "Komik : " . $produk3->getLabel();
echo "</br>";

$produk4 = new Produk("Uncharted", "Neil Druckmann", "Sonny Computer", 250000);
// $produk4->judul = "Uncharted";
// $produk4->penulis = "Neil Druckmann";
// $produk4->penerbit = "Sonny Computer";
// $produk4->harga = 250000;

var_dump($produk4);
echo "Game : " . $produk4->getLabel();
echo "</br>";

$produk5 = new Produk("Dragon Ball");
var_dump($produk5);
