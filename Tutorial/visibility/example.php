<?php

// ? Visibility

// * Konsep yang digunakan untuk mengatur akses terhadap property dan methode pada sebuah object
// * Ada 3 keyword visibility : public, protected, private

// * public : dapat digunakan dimana saja, bahkan diluar class itu sendiri
// * protected : hanya dapat digunakan didalam sebuah class beserta turunanya
// * private : hanya dapat digunakan di dalam sebauh class tertentu saja

// ? Kenapa harus menerapkan konsep visibility ?

// * Hanya memperlihatkan aspek dari class yang dibutuhkan oleh client saja
// * Menentukan kebutuhan yang jelas untuk object
// * Memberikan kendali pada kode untuk menghindari 'bug'
?>
