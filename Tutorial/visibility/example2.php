<?php

// ? Inheritance (pewarisan)

// * Menciptakan hierarki antar class (Parent & Child)
// * Child class, mewarisi semua property dan methode dari class parentnya (yang visible)
// * Child class, memperluas (extends) fungsionalitas dari class parentnya

// ? Kenapa pakai inheritance

class Produk
{
	public $judul, $penulis, $penerbit;
	protected $diskon = 0;
	private $harga;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
	{
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getHarga()
	{
		return $this->harga - ($this->harga * $this->diskon) / 100;
	}

	public function getLabel()
	{
		return "$this->penulis, $this->penerbit";
	}

	public function getInfoProduk()
	{
		$str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";

		return $str;
	}
}

$produk3 = new Komik("Naruto", "Masashi Kishimoto", "Shounen Jump", 300000, 100);

$produk4 = new Game("Uncharted", "Neil Druckmann", "Sonny Computer", 250000, 50);

class Komik extends Produk
{
	public $jmlHalaman = 0;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlHalaman = 0)
	{
		parent::__construct($judul, $penulis, $penerbit, $harga);

		$this->jmlHalaman = $jmlHalaman;
	}

	public function getInfoProduk()
	{
		$str = "Komik : " . parent::getInfoProduk() . " - {$this->jmlHalaman} Halaman.";

		return $str;
	}
}

class Game extends Produk
{
	public $waktuMain = 0;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $waktuMain = 0)
	{
		parent::__construct($judul, $penulis, $penerbit, $harga);

		$this->waktuMain = $waktuMain;
	}

	public function getInfoProduk()
	{
		$str = "Game : " . parent::getInfoProduk() . " ~ {$this->waktuMain} Jam.";

		return $str;
	}

	public function setDiskon($diskon)
	{
		$this->diskon = $diskon;
	}
}

class CetakInfoProduk
{
	public function cetak(Produk $produk)
	{
		$str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";

		return $str;
	}
}

echo $produk3->getInfoProduk();
echo "</br>";
echo $produk4->getInfoProduk();
echo "</br>";

echo "<hr>";

$produk4->setDiskon(50);
echo $produk4->getHarga();
