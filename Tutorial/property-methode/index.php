<?php 

// ? Definisi Property

// * merepresentasikan data / keadaan dari sebuah object
// * variabel yang ada di dalam object (member variable) 

// ? Definisi Methode

// * Merepresentasikan perilaku dari sebuah object
// * function yang ada di dalam object


class Mobil {
    public $nama;
    public $merk;
    public $warna;
    public $kecepatanMaksimal;
    public $jumlahPenumpang;

    public function tambahKecepatan() {

    }

    public function kurangKecepatan() {

    }

    public function gantiTransmisi() {
        
    }
}

?>