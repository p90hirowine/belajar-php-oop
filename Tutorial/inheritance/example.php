<?php

// ? Inheritance (pewarisan)

// * Menciptakan hierarki antar class (Parent & Child)
// * Child class, mewarisi semua property dan methode dari class parentnya (yang visible)
// * Child class, memperluas (extends) fungsionalitas dari class parentnya

// ? Kenapa pakai inheritance

class Produk
{
	public $judul, $penulis, $penerbit, $harga, $jmlHalaman, $waktuMain, $tipe;

	public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlHalaman = 0, $waktuMain = 0, $tipe)
	{
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
		$this->jmlHalaman = $jmlHalaman;
		$this->waktuMain = $waktuMain;
		$this->tipe = $tipe;
	}

	public function getLabel()
	{
		return "$this->penulis, $this->penerbit";
	}

	public function getInfoLengkap()
	{
		$str = "{$this->tipe} : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";

		if ($this->tipe == "komik") {
			$str .= " - {$this->jmlHalaman} Halaman.";
		} elseif ($this->tipe == "game") {
			$str .= " - {$this->waktuMain} Jam.";
		}

		return $str;
	}
}

$produk3 = new Produk("Naruto", "Masashi Kishimoto", "Shounen Jump", 30000, 100, 0, "komik");

$produk4 = new Produk("Uncharted", "Neil Druckmann", "Sonny Computer", 250000, 0, 50, "game");

class CetakInfoProduk
{
	public function cetak(Produk $produk)
	{
		$str = "{$produk->judul} | {$produk->getLabel()} (Rp. {$produk->harga})";

		return $str;
	}
}

echo $produk3->getInfoLengkap();
echo "</br>";
echo $produk4->getInfoLengkap();
